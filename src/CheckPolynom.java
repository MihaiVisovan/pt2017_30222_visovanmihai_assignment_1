import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CheckPolynom {
	
	String polinom;
	String monom = null;
	List<Term> terms = new ArrayList<Term>();
	int coef, exp;
	int i = 0;
	
	String[] check = new String[100];
	String[] coeficient = new String[100];
	String[] exponent = new String[100];

	boolean ok = true;
    Pattern part = Pattern.compile("[+-]{0,1}+[0-9]+[xX]{0,1}+\\^{0,1}+\\({0,1}+[-]{0,1}+[0-9]{0,9}+\\){0,1}");
    
    public CheckPolynom(){
		
	}

	//functia de verificare daca polinomul este introdus corect
    //si de preluare a coeficientilor si exponentilor din fiecare monom
	public Polynom Check(String polinom){
		
	StringTokenizer st1 = new StringTokenizer(polinom, "+-", true);	
	StringTokenizer st = new StringTokenizer(polinom, "+-", true);
	
	if(st1.nextToken().toString().equals("-"))
	{
		monom = st.nextToken().toString() + st.nextToken().toString();
		check[i] = monom;
		i++;
		
	}
	else
	{
		monom = st.nextToken().toString();
		check[i] = monom;
		i++;

	}

	while(st.hasMoreTokens()){
		monom = st.nextToken().toString() + st.nextToken().toString();
		check[i] = monom;
		i++;
	}
	
	
	
	for(int j = 0; j < i; j++)
	{
		Matcher m = part.matcher(check[j]);
		if(!(m.matches()))
		{
			ok = false;
		}
			
	}
	
	if(ok)
	{
		for(int j = 0; j < i; j++)
		{
		
		
		coeficient = polinom.split("[Xx]\\^\\d+\\+?");
		if(Integer.valueOf(coeficient[j]) > 1 || Integer.valueOf(coeficient[j]) < 1){
			
		coef = Integer.valueOf(coeficient[j]);
		}
		else{
			coef = 1;
		}
		
		
		exponent = check[j].split("\\^");
		exp = Integer.valueOf(exponent[1]);
		
				
		Term t = new Term(coef, exp);
		t.transString(coef, exp);
		terms.add(t);
	
	}
	}
	else
	{
		System.out.println("Introduceti un polinom valid");
		return null;
	}
	
	Polynom polinom_1 = new Polynom(terms);
	polinom_1.sortPolynomList();
	return polinom_1;
	}

}

