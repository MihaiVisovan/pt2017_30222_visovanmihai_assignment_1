
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    private final Calculator calculator = new Calculator();

   @Test
    public void addPolynom() throws Exception {
        //2x^2 -1
    	List<Term> terms1 = new ArrayList<Term>();
    	List<Term> terms2 = new ArrayList<Term>();
    	List<Term> terms_result = new ArrayList<Term>();
    	
        Term term11 = new Term(1, 2);
        Term term12 = new Term(1, 2);
        Term term13 = new Term(-1, 0);
        
        term11.transString(1, 2);
        term12.transString(1, 2);
        term13.transString(-1, 0);

        terms1.add(term11);
        terms1.add(term12);
        terms1.add(term13);
        
        Polynom pol1 = new Polynom(terms1);
        pol1.sortPolynomList();
        // -6x + x^2 + 8

        Term term21 = new Term(-6, 1);
        Term term22 = new Term(1, 2);
        Term term23 = new Term(8, 0);
        
        term21.transString(-6, 1);
        term22.transString(1, 2);
        term23.transString(8, 0);

        terms2.add(term21);
        terms2.add(term22);
        terms2.add(term23);
        
        Polynom pol2 = new Polynom(terms2);
        pol2.sortPolynomList();        

        
        Polynom actual = calculator.addPolynom(pol1, pol2);
        actual.sortPolynomList();

        // 3x^2 -6x + 7
        Term expectedTerm1 = new Term(7, 0);
        Term expectedTerm2 = new Term(-6, 1);
        Term expectedTerm3 = new Term(3, 2);
        
        expectedTerm1.transString(7, 0);
        expectedTerm2.transString(-6, 1);
        expectedTerm3.transString(3, 2);

        terms_result.add(expectedTerm1);
        terms_result.add(expectedTerm2);
        terms_result.add(expectedTerm3);

        
        Polynom expected = new Polynom(terms_result);
        expected.sortPolynomList();

        Assert.assertEquals(expected.polinom_list, actual.polinom_list);
    }
    
    
    @Test
    public void subPolynom() throws Exception {
        // 6x^2 - 1
    	List<Term> terms1 = new ArrayList<Term>();
    	List<Term> terms2 = new ArrayList<Term>();
    	List<Term> terms_result = new ArrayList<Term>();
    	
        Term term11 = new Term(-1, 0);
        Term term12 = new Term(3, 2);
        Term term13 = new Term(3, 2);
        
        term11.transString(-1, 0);
        term12.transString(3, 2);
        term13.transString(3, 2);

        terms1.add(term11);
        terms1.add(term12);
        terms1.add(term13);
        
        Polynom pol1 = new Polynom(terms1);
        pol1.sortPolynomList();
        // -6x + x^2 + 8

        Term term21 = new Term(-3, 1);
        Term term22 = new Term(1, 2);
        Term term23 = new Term(-8, 0);
        
        term21.transString(-3, 1);
        term22.transString(1, 2);
        term23.transString(-8, 0);

        terms2.add(term21);
        terms2.add(term22);
        terms2.add(term23);
        
        Polynom pol2 = new Polynom(terms2);
        pol2.sortPolynomList();      
        //-3x + x^2 - 8
    
        Polynom actual = calculator.subPolynom(pol1, pol2);
        actual.sortPolynomList();

        // 5x^2 +3x +7
        
        Term expectedTerm1 = new Term(3, 1);
        Term expectedTerm2 = new Term(7, 0);
        Term expectedTerm3 = new Term(5, 2);

      
        expectedTerm1.transString(3, 1);
        expectedTerm2.transString(7, 0);
        expectedTerm3.transString(5, 2);


        terms_result.add(expectedTerm1);
        terms_result.add(expectedTerm2);
        terms_result.add(expectedTerm3);

                
        Polynom expected = new Polynom(terms_result);
        expected.sortPolynomList();
        Assert.assertEquals(expected.polinom_list, actual.polinom_list);
    }

@Test
public void mulPolynom() throws Exception {
   
	List<Term> terms1 = new ArrayList<Term>();
	List<Term> terms2 = new ArrayList<Term>();
	List<Term> terms_result = new ArrayList<Term>();
	
    Term term11 = new Term(1, 2);
    Term term12 = new Term(2, 2);
    Term term13 = new Term(-1, 0);
    
    term11.transString(1, 2);
    term12.transString(2, 2);
    term13.transString(-1, 0);

    terms1.add(term11);
    terms1.add(term12);
    terms1.add(term13);
    
    Polynom pol1 = new Polynom(terms1);
    pol1.sortPolynomList();
    //3x^2 -1
    
    Term term21 = new Term(-6, 1);
    Term term22 = new Term(1, 2);
    Term term23 = new Term(8, 0);
    
    term21.transString(-6, 1);
    term22.transString(1, 2);
    term23.transString(8, 0);
    
    terms2.add(term21);
    terms2.add(term22);
    terms2.add(term23);
    
    Polynom pol2 = new Polynom(terms2);
    pol2.sortPolynomList();    
    // x^2 - 6x + 8
    
    Polynom actual = calculator.mulPolynom(pol1, pol2);
    actual.sortPolynomList();
    // 3^4 - 18x^3 + 23x^2 + 6x - 8

    Term expectedTerm1 = new Term(3, 4);
    Term expectedTerm2 = new Term(-18, 3);
    Term expectedTerm3 = new Term(23, 2);
    Term expectedTerm4 = new Term(6, 1);
    Term expectedTerm5 = new Term(-8, 0);

    expectedTerm1.transString(3, 4);
    expectedTerm2.transString(-18, 3);
    expectedTerm3.transString(23, 2);
    expectedTerm4.transString(6, 1);
    expectedTerm5.transString(-8, 0);

    terms_result.add(expectedTerm1);
    terms_result.add(expectedTerm2);
    terms_result.add(expectedTerm3);
    terms_result.add(expectedTerm4);
    terms_result.add(expectedTerm5);

            
    Polynom expected = new Polynom(terms_result);
    expected.sortPolynomList();

    Assert.assertEquals(expected.polinom_list, actual.polinom_list);
}




@Test
public void derPolynom() throws Exception {
   
	List<Term> terms1 = new ArrayList<Term>();
	List<Term> terms_result = new ArrayList<Term>();
	
    Term term11 = new Term(3, 2);
    Term term12 = new Term(3, 2);
    Term term13 = new Term(-5, 0);
    
    term11.transString(3, 2);
    term12.transString(3, 2);
    term13.transString(-5, 0);

    terms1.add(term11);
    terms1.add(term12);
    terms1.add(term13);
    
    Polynom pol1 = new Polynom(terms1);
    pol1.sortPolynomList();
    //6x^2 -5

    Polynom actual = calculator.derPolynom(pol1);
    actual.sortPolynomList();
 
    Term expectedTerm1 = new Term(12, 1);
    Term expectedTerm2 = new Term(0, 0);
 
    expectedTerm1.transString(6, 1);
    expectedTerm2.transString(2, 0);
 
    terms_result.add(expectedTerm1);
    terms_result.add(expectedTerm2);

            
    Polynom expected = new Polynom(terms_result);
    expected.sortPolynomList();

    Assert.assertEquals(expected.polinom_list, actual.polinom_list);
}

@Test
public void intPolynom() throws Exception {
   
	List<TermD> terms1 = new ArrayList<TermD>();
	List<TermD> terms_result = new ArrayList<TermD>();
	
    TermD term11 = new TermD(3, 2);
    TermD term12 = new TermD(3, 2);
    TermD term13 = new TermD(-5, 0);
    
    term11.transString(3, 2);
    term12.transString(3, 2);
    term13.transString(-5, 0);

    terms1.add(term11);
    terms1.add(term12);
    terms1.add(term13);
    
    PolynomDouble pol1 = new PolynomDouble(terms1);
    pol1.sortPolynomList();
    //6x^2 -5
    PolynomDouble actual = calculator.intPolynom(pol1);
    actual.sortPolynomList();

    TermD expectedTerm1 = new TermD(2.0, 3);
    TermD expectedTerm2 = new TermD(-5.0, 1);
 
    expectedTerm1.transString(2.0, 3);
    expectedTerm2.transString(-5.0, 1);
 
    terms_result.add(expectedTerm1);
    terms_result.add(expectedTerm2);

            
    PolynomDouble expected = new PolynomDouble(terms_result);
    expected.sortPolynomList();


    Assert.assertEquals(expected.polinom_list, actual.polinom_list);
}
}
