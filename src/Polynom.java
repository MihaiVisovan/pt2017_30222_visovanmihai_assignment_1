import java.util.Collections;
import java.util.List;
import java.util.ArrayList;


public class Polynom {

		

	List <Term> terms = new ArrayList<Term>();
	List <Term> terms_final = new ArrayList<Term>();

	String polinom_list = "";
	int coef, exp;
		
	Polynom(){
		
	}
	
	Polynom(List <Term> terms){
		this.terms = terms;
		
	}
	
	
//functia de sortare a polinomului si de reducere a termenilor reductibli introdusi
	public void sortPolynomList(){
		Collections.sort(terms);
		Collections.reverse(terms);
		
	for(Term t: terms){
			coef = t.getCoef();
			boolean ok = true;
			for(Term t1: terms){
				if(t != t1 && t.getExp() == t1.getExp())
				{
					coef += t1.getCoef();
				}
			}
			for(Term t2: terms_final){
				if(t2.getExp() == t.getExp()){
					ok = false;
				}
			}
			if(ok){
			Term newTerm2 = new Term(coef, t.getExp());
			newTerm2.transString(coef,t.getExp());
			terms_final.add(newTerm2);
			}

		}
		terms.clear();
		terms.addAll(terms_final);
		for(Term t: terms){
			polinom_list = polinom_list + t.getTerm();
		}
		
	}

	}

