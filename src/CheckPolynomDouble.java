
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CheckPolynomDouble {
	
	String polinom;
	String monom = null;
	List<TermD> terms = new ArrayList<TermD>();
	int coef, exp;
	int i = 0;
	
	String[] check = new String[100];
	String[] coeficient = new String[100];
	String[] exponent = new String[100];

	boolean ok = true;
    Pattern part = Pattern.compile("[+-]{0,1}+[0-9]+[xX]{0,1}+\\^{0,1}+\\({0,1}+[-]{0,1}+[0-9]{0,9}+\\){0,1}");
    
    public CheckPolynomDouble(){
		
	}

	
	public PolynomDouble Check(String polinom){
		
	StringTokenizer st1 = new StringTokenizer(polinom, "+-", true);	
	StringTokenizer st = new StringTokenizer(polinom, "+-", true);
	
	if(st1.nextToken().toString().equals("-"))
	{
		monom = st.nextToken().toString() + st.nextToken().toString();
		check[i] = monom;
		i++;
		
	}
	else
	{
		monom = st.nextToken().toString();
		check[i] = monom;
		i++;

	}

	while(st.hasMoreTokens()){
		monom = st.nextToken().toString() + st.nextToken().toString();
		check[i] = monom;
		i++;
	}
	
	
	
	for(int j = 0; j < i; j++)
	{
		Matcher m = part.matcher(check[j]);
		if(!(m.matches()))
		{
			ok = false;
		}
			
	}
	
	if(ok)
	{
		for(int j = 0; j < i; j++)
		{
		
		
		coeficient = polinom.split("[Xx]\\^\\d+\\+?");
		coef = Integer.valueOf(coeficient[j]);
		//System.out.println(coef + "\n");
		
		exponent = check[j].split("\\^");
		exp = Integer.valueOf(exponent[1]);
		//System.out.println(exp);
		
				
		TermD t = new TermD(coef, exp);
		t.transString(coef, exp);
		terms.add(t);
	
	}
	}
	else
	{
		System.out.println("Introduceti un polinom valid");
		return null;
	}
	
	PolynomDouble polinom_1 = new PolynomDouble(terms);
	polinom_1.sortPolynomList();
	return polinom_1;
	}

}

