import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class PolynomDouble {

		

	List <TermD> terms = new ArrayList<TermD>();
	List <TermD> terms_final = new ArrayList<TermD>();

	String polinom_list = "";
	double coef;
	int exp;
		
	PolynomDouble(){
	}
	
	PolynomDouble(List <TermD> terms){
		this.terms = terms;
		
	}
	
	//aceeasi functie ca si in clasa Polynom doar cu coeficienti double
	public void sortPolynomList(){
		Collections.sort(terms);
		Collections.reverse(terms);
		
	for(TermD t: terms){
			coef = t.getCoef();
			boolean ok = true;
			for(TermD t1: terms){
				if(t != t1 && t.getExp() == t1.getExp())
				{
					coef += t1.getCoef();
				}
			}
			for(TermD t2: terms_final){
				if(t2.getExp() == t.getExp()){
					ok = false;
				}
			}
			if(ok){
			TermD newTerm2 = new TermD(coef, t.getExp());
			newTerm2.transString(coef,t.getExp());
			terms_final.add(newTerm2);
			}

		}
		terms.clear();
		terms.addAll(terms_final);
		for(TermD t: terms){
			polinom_list = polinom_list + t.getTerm();
		}
		
	}

	}

