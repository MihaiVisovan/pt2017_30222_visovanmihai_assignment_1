
public class Term implements Comparable<Term>{


	private int coef;
	private int exp;
	private String coef_1;
	private String exp_1;
	private String term;
	private String term1 = "X^";
	private String term2 = "X";
	private String input;
	
	public Term(){
		
	}
	
	public Term(int coef, int exp)
	{
		this.coef = coef;
		this.exp = exp;
	}
	
	public Term(String input){
		this.input = input;
		
	}
	
	public Term(String coef_1, String exp_1, String term){
		this.coef_1 = coef_1;
		this.exp_1 = exp_1;
		this.term = term;
	}
	//functia de transformare in String cu toate cazurile posibile
	public void transString(int coef, int exp){
		
		coef_1 = String.valueOf(coef);
		exp_1 = String.valueOf(exp);
		
		
		term = "+" + coef_1 + term1 + exp_1;

	
		if(coef < 0 && exp < 0){
			term = coef_1 + term1 + "(" + exp_1 + ")";
		}
		
		if(coef < 0 && exp == 0){
			term = coef_1;
		}
		
		if(coef < 0 && exp == 1){
			term = coef_1 + term2;
		}
		
		if(coef < 0 && exp > 1){
			term = coef_1 + term1 + exp_1;
		}
	
		if(coef == -1 && exp < 0){
			term = "-" + term1 + "(" + exp_1 + ")";
			}
		if(coef == - 1 && exp == 1){
			term = "-" + term2;
		}
		if(coef == -1 && exp > 1){
			term = "-" + term1 + exp_1;
		}
		
		if(coef == 1 && exp < 0){
			term = "+" + term1 +  "(" + exp_1 + ")";
		}
		
		if(coef == 1 && exp == 0){
			term = "+" + coef_1;
		}
		
		if(coef == 1 && exp == 1){
			term = "+" + term2;
		}
		
		if(coef == 1 && exp > 1){
			term = "+" + term1 + exp_1;
		}
		
		if(coef > 1 && exp < 0){
			term = "+" + coef_1 + term1 + "(" + exp_1 + ")";
 		}
		
		if(coef > 1 && exp == 0){
			term = "+" + coef_1;
		}
		
		if(coef > 1 && exp == 1){
			term = "+" + coef_1 + term2;
		}	
		if(coef == 0){
			term = "";
		}
			
		
	}
	
	
	public int getCoef() {
		return coef;
	}
	public int getExp() {
		return exp;
	}

	public String getTerm() {
		return term;
	}
	
	public String getInput() {
		return input;
	}
	
	public String getCoef_1() {
		return coef_1;
	}
	public void setCoef(int coef) {
		this.coef = coef;
	}

	public String getExp_1() {
		return exp_1;
	}
	@Override
	public int compareTo(Term t) {
		return this.getExp_1().compareTo(t.getExp_1());
    }

 }


