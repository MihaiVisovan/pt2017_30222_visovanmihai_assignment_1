import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;


public class Calculator {

	
//	Polynom pol_1 = new Polynom();
//	Polynom pol_2 = new Polynom();
//	List<Term> terms_1 = new ArrayList<Term>();
//	List<Term> terms_final = new ArrayList<Term>();
//	List<TermD> terms_final_double = new ArrayList<TermD>();
//	int exp, coef;
//	double coef_double;
	
	
	public Polynom addPolynom(Polynom pol_1, Polynom pol_2){
		Iterator<Term> iter_1 = pol_1.terms.iterator();
		List<Term> terms_1 = new ArrayList<Term>();
		List<Term> terms_final = new ArrayList<Term>();
		int coef;
		while(iter_1.hasNext()){
			Term t = iter_1.next();
			Iterator<Term> iter_2 = pol_2.terms.iterator();
			while(iter_2.hasNext()){
				Term t1 = iter_2.next();
				if(t.getExp() == t1.getExp()){
					coef = t.getCoef() + t1.getCoef();
					Term term_1 = new Term(coef, t.getExp());
					term_1.transString(coef, t.getExp());
					terms_1.add(term_1);
				}
			}
		}
		pol_1.terms.addAll(pol_2.terms);
		terms_final.addAll(terms_1);
		
	for(Term t: pol_1.terms){
		boolean ok = true;
		
		for(Term t1: terms_1)
			if(t.getExp() == t1.getExp()){
				ok = false;	
		}
		if(ok)
		{
			terms_final.add(t);
		}
	}
		
		
		Polynom pol_r = new Polynom(terms_final);
		return pol_r;
	}
	
	public Polynom subPolynom(Polynom pol_1, Polynom pol_2){
		Iterator<Term> iter_1 = pol_1.terms.iterator();
		
		List<Term> terms_1 = new ArrayList<Term>();
		List<Term> terms_final = new ArrayList<Term>();
		int coef;
		while(iter_1.hasNext()){
			Term t = iter_1.next();
			Iterator<Term> iter_2 = pol_2.terms.iterator();
			while(iter_2.hasNext()){
				Term t1 = iter_2.next();
				if(t.getExp() == t1.getExp()){
					coef = t.getCoef() - t1.getCoef();
					Term term_1 = new Term(coef, t.getExp());
					term_1.transString(coef, t.getExp());
					terms_1.add(term_1);
				}
			}
		}
		
	for(Term t: pol_2.terms)
	{
		coef = t.getCoef();
		t.setCoef(-coef);
	}
	
		pol_1.terms.addAll(pol_2.terms);
		terms_final.addAll(terms_1);
		
	for(Term t: pol_1.terms){
		boolean ok = true;
		
		for(Term t1: terms_1)
			if(t.getExp() == t1.getExp()){
				ok = false;	
		}
		if(ok)
		{
			terms_final.add(t);
		}
	}
		
		
		Polynom pol_r = new Polynom(terms_final);
		return pol_r;
	}



public Polynom mulPolynom(Polynom pol_1, Polynom pol_2){
	
	List<Term> terms_1 = new ArrayList<Term>();
	List<Term> terms_final = new ArrayList<Term>();
	int exp , coef;
	for(Term t: pol_1.terms){
		for(Term t1: pol_2.terms)
		{
			exp = t.getExp() + t1.getExp();
			coef = t.getCoef()*t1.getCoef();
			Term newTerm = new Term(coef, exp);
			newTerm.transString(coef, exp);
			terms_1.add(newTerm);
		}

	}
	
	for(Term t: terms_1){
		coef = t.getCoef();
		boolean ok = true;
		for(Term t1: terms_1){
			if(t != t1 && t.getExp() == t1.getExp())
			{
				coef += t1.getCoef();
			}
		}
		for(Term t2: terms_final){
			if(t2.getExp() == t.getExp()){
				ok = false;
			}
		}
		if(ok){
		Term newTerm2 = new Term(coef, t.getExp());
		newTerm2.transString(coef,t.getExp());
		terms_final.add(newTerm2);
		}

	}
	Polynom pol_r = new Polynom(terms_final);
	return pol_r;
}


public Polynom derPolynom(Polynom pol_1)
{
	List<Term> terms_final = new ArrayList<Term>();
	int exp , coef;
	
	for(Term t: pol_1.terms)
	{
		if(t.getExp() == 0)
		{
			coef = 0;
			exp = t.getExp();
		}
		else
		{
			coef = t.getCoef() * (t.getExp());
			exp = t.getExp() - 1;
		}
		Term newTerm3 = new Term(coef, exp);
		newTerm3.transString(coef, exp);
		terms_final.add(newTerm3);
		
	}
		
	Polynom pol_r = new Polynom(terms_final);
	return pol_r;
	
}


public PolynomDouble intPolynom(PolynomDouble pol_1)
{

	List<TermD> terms_final_double = new ArrayList<TermD>();
	int exp,  coef;
	double coef_double = 0;
	for(TermD t: pol_1.terms)
	{
		if(t.getCoef() == 0)
		{
			coef = 0;
			exp = t.getExp();
		}
		else{
			
		coef_double = t.getCoef() / (t.getExp()+1);
		exp = t.getExp() + 1;
		}
		
		TermD newTerm4 = new TermD(coef_double, exp);
		newTerm4.transString(coef_double, exp);
		terms_final_double.add(newTerm4);
	
	}
	PolynomDouble pol_r = new PolynomDouble(terms_final_double);
	return pol_r;

}
}
