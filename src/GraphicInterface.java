import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;



import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class GraphicInterface {



	private JFrame frame;
	private JTextField FirstPolynom;
	private JTextField SecondPolynom;
	private JTextField Addition;
	private JTextField Subtraction;
	private JTextField Multiplication;
	private JTextField Derivation;
	private JTextField Integration;
	private JButton btnReset;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphicInterface window = new GraphicInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GraphicInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Calculator calculator = new Calculator();
		//Calculator calculator1 = new Calculator();
	
		frame = new JFrame();
		frame.setBounds(100, 100, 604, 337);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("The first polynom is:");
		lblNewLabel.setBounds(137, 27, 115, 25);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("The second polynom is:");
		lblNewLabel_1.setBounds(137, 63, 137, 25);
		frame.getContentPane().add(lblNewLabel_1);
		
		FirstPolynom = new JTextField();
		FirstPolynom.setBounds(278, 28, 127, 23);
		frame.getContentPane().add(FirstPolynom);
		FirstPolynom.setColumns(10);
		
		SecondPolynom = new JTextField();
		SecondPolynom.setBounds(278, 64, 127, 23);
		frame.getContentPane().add(SecondPolynom);
		SecondPolynom.setColumns(10);
		

		//la fiecare buton initializam String-urile cu cele 2polinoame, 
		//le verificam corectitudinea, il sortam, il reducem si afisam
		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					
					String polinom_1 = FirstPolynom.getText();
					String polinom_2 = SecondPolynom.getText();
					
					CheckPolynom check = new CheckPolynom();
					Polynom pol_1 = check.Check(polinom_1);
					//System.out.println(pol_1.polinom_list);
					
					CheckPolynom check1 = new CheckPolynom();
					Polynom pol_2 = check1.Check(polinom_2);
					//System.out.println(pol_2.polinom_list);
					
					Polynom pol_add = new Polynom();
					 pol_add = calculator.addPolynom(pol_1, pol_2);
					pol_add.sortPolynomList();
					//System.out.println(pol_add.polinom_list);
					Addition.setText(pol_add.polinom_list);
					}catch(Exception f){JOptionPane.showMessageDialog(null,"Introduceti un polinom valid");}
			}
		});
		btnAdd.setBounds(26, 131, 101, 23);
		frame.getContentPane().add(btnAdd);
		
		
		
		JButton btnSubtraction = new JButton("SUB");
		btnSubtraction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					String polinom_3 = FirstPolynom.getText();
					String polinom_4 = SecondPolynom.getText();

					CheckPolynom check2 = new CheckPolynom();
					Polynom pol_3 = check2.Check(polinom_3);
					
					CheckPolynom check3 = new CheckPolynom();
					Polynom pol_4 = check3.Check(polinom_4);
			
					Polynom pol_sub = new Polynom();
				    pol_sub = calculator.subPolynom(pol_3, pol_4);
				    
					pol_sub.sortPolynomList();
				    System.out.println(pol_sub.polinom_list);

					Subtraction.setText(pol_sub.polinom_list);
					}catch(Exception f){JOptionPane.showMessageDialog(null,"Introduceti un polinom valid");}
			}
		});
		
			
		btnSubtraction.setBounds(26, 165, 101, 23);
		frame.getContentPane().add(btnSubtraction);
		

		
		JButton btnNewButton = new JButton("MULTIPLY");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String polinom_1 = FirstPolynom.getText();
					String polinom_2 = SecondPolynom.getText();

					CheckPolynom check = new CheckPolynom();
					Polynom pol_1 = check.Check(polinom_1);
					System.out.println(pol_1.polinom_list);
				
					CheckPolynom check1 = new CheckPolynom();
					Polynom pol_2 = check1.Check(polinom_2);
					System.out.println(pol_2.polinom_list);

					Polynom pol_mul = calculator.mulPolynom(pol_1, pol_2);
					pol_mul.sortPolynomList();
					System.out.println(pol_mul.polinom_list);
					Multiplication.setText(pol_mul.polinom_list);
					}catch(Exception f){JOptionPane.showMessageDialog(null,"Introduceti un polinom valid");}
			}
		});
		btnNewButton.setBounds(26, 199, 101, 23);
		frame.getContentPane().add(btnNewButton);
		
	
	
		JButton btnNewButton_1 = new JButton("DERIVATE");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String polinom_1 = FirstPolynom.getText();

					CheckPolynom check = new CheckPolynom();
					Polynom pol_1 = check.Check(polinom_1);
					System.out.println(pol_1.polinom_list);
				

					Polynom pol_add = calculator.derPolynom(pol_1);
					pol_add.sortPolynomList();
					System.out.println(pol_add.polinom_list);
					Derivation.setText(pol_add.polinom_list);
					}catch(Exception f){JOptionPane.showMessageDialog(null,"Introduceti un polinom valid");}
			}
		});
		btnNewButton_1.setBounds(303, 145, 115, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		
		JButton btnNewButton_2 = new JButton("INTEGRATE");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					String polinom_1 = FirstPolynom.getText();

					CheckPolynomDouble check = new CheckPolynomDouble();
					PolynomDouble pol_1 = check.Check(polinom_1);
					System.out.println(pol_1.polinom_list);
				

					PolynomDouble pol_add = calculator.intPolynom(pol_1);
					pol_add.sortPolynomList();
					System.out.println(pol_add.polinom_list);
					Integration.setText(pol_add.polinom_list);
					}catch(Exception f){JOptionPane.showMessageDialog(null,"Introduceti un polinom valid");}
			}
		});
		btnNewButton_2.setBounds(303, 179, 115, 23);
		frame.getContentPane().add(btnNewButton_2);
		
		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integration.setText(null);
				Derivation.setText(null);
				Multiplication.setText(null);
				Addition.setText(null);
				Subtraction.setText(null);
				FirstPolynom.setText(null);;
				SecondPolynom.setText(null);
			}
		});
		btnReset.setBounds(316, 249, 89, 23);
		frame.getContentPane().add(btnReset);
		
	
		Subtraction = new JTextField();
		Subtraction.setBounds(137, 166, 137, 20);
		frame.getContentPane().add(Subtraction);
		Subtraction.setColumns(10);
		


		
		Addition = new JTextField();
		Addition.setBounds(137, 131, 137, 21);
		frame.getContentPane().add(Addition);
		Addition.setColumns(10);
		
		Multiplication = new JTextField();
		Multiplication.setBounds(137, 200, 137, 20);
		frame.getContentPane().add(Multiplication);
		Multiplication.setColumns(10);
		
		Derivation = new JTextField();
		Derivation.setBounds(444, 146, 134, 20);
		frame.getContentPane().add(Derivation);
		Derivation.setColumns(10);
		
		Integration = new JTextField();
		Integration.setBounds(444, 180, 134, 20);
		frame.getContentPane().add(Integration);
		Integration.setColumns(10);
		
	
	
	
	}


}
